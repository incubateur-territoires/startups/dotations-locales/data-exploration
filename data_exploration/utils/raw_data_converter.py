import pandas as pd

PATH = "./data/"


def convert_raw_criteres_data(criteres_data_path, year):
    """La fonction convertit le fichier excel contenant les données brutes des critères de repartition de la DGCL en un fichier csv prêt à être utilisé.
    Voici les opérations effectuées :
    - Suppression des deux premières lignes vides
    - Suppression de la première colonne vide
    - Concatenation des 2 premières lignes en une seule ligne qui constitue le header
    Le paramètre d'entrée est le chemin du fichier excel.
    La fonction retourne un statut de succès ou d'échec."""

    # Les fichiers n'étant pas les mêmes pour tous les années, on doit donc les charger différemment
    if year == 2021:
        read_file = pd.read_excel(criteres_data_path)
        # drop 2 first lines and first column
        read_file = read_file.drop(read_file.index[:1])
        read_file = read_file.drop(read_file.columns[0], axis=1)
    elif year == 2022:
        read_file = pd.read_excel(criteres_data_path, sheet_name=1)
        # drop 2 first lines and first column
        read_file = read_file.drop(read_file.index[:2])
        read_file = read_file.drop(read_file.columns[0], axis=1)
    elif year == 2020:
        read_file = pd.read_excel(criteres_data_path, header=None)
    else:
        print("Year not supported")
        return False

    
    

    # merge the 2 first rows in one and set it as header
    first_row = read_file.iloc[0]
    print(first_row)
    second_row = read_file.iloc[1]
    new_column_name = first_row + " - " + second_row
    read_file.rename(columns=new_column_name, inplace=True)

    # drop the merged rows
    read_file = read_file.iloc[2:]

    # save the file as csv
    criteres_data_path_out = criteres_data_path.replace(".xlsx", ".csv")
    read_file.to_csv(criteres_data_path_out, index=None, header=True)

    return True


# TO DO : trouver le bon engine pour lire le format de fichier excel

# def convert_raw_dotations_data(dotations_data_path):
#     read_file = pd.read_excel(dotations_data_path, engine="xlrd")
#     dotations_data_path_out = dotations_data_path.replace(".xlsx", ".csv")
#     read_file.to_csv(dotations_data_path_out, index=None, header=True)


if __name__ == "__main__":
    convert_raw_criteres_data(PATH + "criteres_repartition_2020.xlsx", 2020)
    print("Conversion terminée.")
