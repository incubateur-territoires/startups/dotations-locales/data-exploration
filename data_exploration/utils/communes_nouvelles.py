from pandas import DataFrame, concat

# Communes nouvelles 2021
# https://fr.wikipedia.org/wiki/Liste_des_communes_nouvelles_créées_en_2021

# code insee 2021 = 16233 - Mosnac-Saint-Simeux - Chef lieu : Mosnac
# était : Mosnac + Saint-Simeux
source_mosnac_saint_simeux = {
    "Nom de la commune": ["MOSNAC", "SAINT-SIMEUX", "MOSNAC-SAINT-SIMEUX"],
    "Code INSEE de la commune": ["16233", "16351", "16233"],
    "Date début (janvier)": [None, None, "2021"],
    "Date fin (décembre)": ["2020", "2020", None],
    "Code INSEE évolution": ["16233", "16233", None],  # pivot commune après
    "Raison évolution": ["merge", "merge", None]
}
df_mosnac_saint_simeux = DataFrame(data=source_mosnac_saint_simeux)

# code insee 2021 = 53249 - Vimartin-sur-Orthe - Chef lieu : Saint-Pierre-sur-Orthe
# était : Saint-Martin-de-Connée, Saint-Pierre-sur-Orthe et Vimarcé

source_vimartin_sur_orthe = {
    "Nom de la commune": ["SAINT-MARTIN-DE-CONNEE", "SAINT-PIERRE-SUR-ORTHE", "VIMARCE", "VIMARTIN-SUR-ORTHE"],
    "Code INSEE de la commune": [None, None, None, "53249"],
    "Date début (janvier)": [None, None, None, "2021"],
    "Date fin (décembre)": ["2020", "2020", "2020", None],
    "Code INSEE évolution": ["53249", "53249", "53249", None],  # pivot commune après
    "Raison évolution": ["merge", "merge", "merge", None]
}
df_vimartin_sur_orthe = DataFrame(data=source_vimartin_sur_orthe)

df_communes_nouvelles = concat([df_mosnac_saint_simeux, df_vimartin_sur_orthe])
