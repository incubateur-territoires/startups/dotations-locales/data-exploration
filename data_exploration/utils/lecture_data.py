from os import getcwd
from pandas import read_csv
from data_exploration.utils.utils import load_dgcl_file


# LISTE DES DONNEES
TMP_OLD_DATA_DIRECTORY = getcwd() + "/../../data/"  
# TODO: basculer vers DATA_DIRECTORY lorsque la dépendance à dotations_locales_back sera rompue
# voir analyse_2022.ipynb, exploration.ipynb, simulation_2021.ipynb, simulation_dsr.ipynb...
DATA_DIRECTORY = getcwd() + "/data/dotations/"

# DGCL
CRITERES_2018_PATH = DATA_DIRECTORY + "2018-communes-criteres-repartition.csv"
CRITERES_2019_PATH = DATA_DIRECTORY + "2019-communes-criteres-repartition.csv"
CRITERES_2020_PATH = TMP_OLD_DATA_DIRECTORY + "criteres_repartition_2020.csv"
CRITERES_2021_PATH = TMP_OLD_DATA_DIRECTORY + "criteres_repartition_2021.csv"
CRITERES_2022_PATH = TMP_OLD_DATA_DIRECTORY + "criteres_repartition_2022.csv"

# Garanties DSU estimées
GARANTIES_DSU_PATH = TMP_OLD_DATA_DIRECTORY + "garanties_dsu.csv"


# CHARGEMENT DES DONNEES
criteres_repartition_2020 = load_dgcl_file(CRITERES_2020_PATH)
criteres_repartition_2021 = load_dgcl_file(CRITERES_2021_PATH)
criteres_repartition_2022 = load_dgcl_file(CRITERES_2022_PATH)

garanties_dsu = read_csv(GARANTIES_DSU_PATH, dtype={"Informations générales - Nom de la commune": str})
