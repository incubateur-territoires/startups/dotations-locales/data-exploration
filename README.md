# data-exploration

Analyse des données ouvertes et calculées sur les dotations de l'État aux territoires.

## Installation

Ce dépôt requiert le [langage Python](https://www.python.org) dans sa version `3.11`.

> Vous pouvez définir une version Python locale, propre à ce dépôt avec [pyenv](https://github.com/pyenv/pyenv).  
> Les commandes sont alors `pyenv install 3.11` suivie de `pyenv local 3.11`.

Pour installer les dépendances de ce dépôt, exécuter les commandes suivantes dans un terminal Shell : 
```shell
poetry env use python3.11
poetry install
```

Ceci créera un environnement virtuel.  
Si l'on souhaite voir l'environnement actif, celui-ci est indiqué parmi les environnements existants. Cette commande permet de les lister :
```shell
poetry env list
```

## Exécution

Ce dépôt est constitué d'un ensemble de [notebooks Jupyter](https://jupyter.org) et de scripts auxiliaires.

La commande suivante permet d'exécuter ces notebooks en local :
```shell
poetry run jupyter notebook
```

Ils seront alors ouverts dans votre navigateur favori.
