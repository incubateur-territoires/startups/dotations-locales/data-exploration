# CHANGELOG

## 3.1.0 [!8](https://gitlab.com/incubateur-territoires/startups/dotations-locales/data-exploration/-/merge_requests/8/diffs#3206e3e18e4e47c67e7599927ad054675d909ee6)

* Ajoute des calculs.
* Périmètre métier : territoires communaux
* Période : 2022
* Détails :
  * Affiche en un tableau des écarts de calcul 2022 pour DF, DSR et DSU des communes via `data_exploration/dotations_2022/analyse_2022.ipynb`
  * Ajoute des utilitaires de présentation des résultats dans `data_exploration/utils/design.py`

# 3.0.0 [!7](https://gitlab.com/incubateur-territoires/startups/dotations-locales/data-exploration/-/merge_requests/7)

* Amélioration technique non rétro-compatible.
* Périmètre métier : DGF communes (DF, DSR, DSU)
* Période : 2021, 2022
* Détails :
  * Passe de Python `3.8` à Python `3.11`
  * Met à jour `openfisca-france-dotations-locales` de `0.8.1` à `3.*.*`
  * Retire la dépendance à `dotations-locales-back`
    * Pour 2021 et 2022, intègre les mappings entre colonnes de fichiers de critères DGCL et variables OpenFisca 
    * Le fichier `data_exploration/utils/new_load_dgcl_criteres_data.py` regroupe des fonctions qui ont évolué entre 2021 et 2022

# 2.0.0 [!6](https://gitlab.com/incubateur-territoires/startups/dotations-locales/data-exploration/-/merge_requests/6)

* Ajoute des calculs.
* Périmètre métier : territoires communaux
* Période : 2023
* Détails :
  * Ajoute l'analyse de l'évolution des communes en 2023
    * A servi à la mise à jour de l'[API web territoires](https://git.leximpact.dev/leximpact/territoires/territoires)
  * Dans `data/` crée un répertoire `dotations/` et y déplace les données du calcul de dotations
    * Exclut temporairement les données dont le chemin est fixe dans la dépendance dotations-locales-back
  * Dans `data/` crée un répertoire `territoires/` et y déplace les données de suivi des évolutions des communes
  * Met à jour les chemins des données déplacées dans les notebooks et scripts de toutes les années
  * Initie un module Python `data_exploration` rassemblant les notebookds pouvant être déplacés sans régression de calcul

## 1.3.0 [!5](https://gitlab.com/incubateur-territoires/startups/dotations-locales/data-exploration/-/merge_requests/5)

* Ajoute des calculs.
* Périmètre métier : DSR
* Période : 2022
* Détails :
  * Ajoute un notebook sur la base de la note DGCL 2022 pour chacune des fractions de DSR

## 1.2.0 [!4](https://gitlab.com/incubateur-territoires/startups/dotations-locales/data-exploration/-/merge_requests/4)

* Ajoute des calculs.
* Périmètre métier : DSR bourg-centre
* Période : 2019, 2021, 2022
* Détails :
  * Ajoute les données 2019 de garanties communes nouvelles pour la DSR
  * Ajoute les données 2022 de notification de DF, DSR toutes fractions et DSU 
  * Initie un notebook `simulation_2021.ipynb` pour socle de la simulation de l'année suivante
  * Initie un notebook `simulation_dsr.ipynb` pour le debug de la fraction bourg-centre de la DSR 2022
    * Ajoute une dépendance à `dotations_locales_back` et des fonctions auxiliaires dans `utils/utils.py`

## 1.1.0 [!3](https://gitlab.com/incubateur-territoires/startups/dotations-locales/data-exploration/-/merge_requests/3)

* Ajoute des calculs.
* Périmètre métier : DGF communes (DF, DSR, DSU)
* Période : 2021, 2022
* Détails : 
  * Ajoute la conversion des fichiers .xls DGCL en .csv dans `utils/`
  * Ajoute le chargement des données de dotations et critères dans `utils/`
  * Initie un notebook `exploration.ipynb` qui regroupe les données chargées en un DataFrame

# 1.0.0 [!2](https://gitlab.com/incubateur-territoires/startups/dotations-locales/data-exploration/-/merge_requests/2)

* Ajoute des calculs.
* Périmètre métier : DSU
* Période : DSU 2022 et données 2018, 2019, 2020, 2022
* Détails :
  * Ajoute un notebook `note_dgcl_dsu_2022.ipynb` sur la base de la [note DGCL 2022 pour la DSU](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=179) et y joint les calculs de l'éligibilité
  * Analyse les résultats obtenus dans `analyse_2022.ipynb` et les garanties en particulier dans `analyse_garanties_dsu.ipynb`
  * Ajoute une configuration `poetry` pour installer les dépendances du dépôts en Python `3.8`
  * Ajoute ce `CHANGELOG` pour le suivi des principales évolutions
